function shuffle(arr) {
  var ctr = arr.length, temp, index;

// While there are elements in the array
  while (ctr > 0) {
// Pick a random index
      index = Math.floor(Math.random() * ctr);
// Decrease ctr by 1
      ctr--;
// And swap the last element with it
      temp = arr[ctr];
      arr[ctr] = arr[index];
      arr[index] = temp;
  }
  return arr;
}

let notes=[["A4","440"],["F4","349.23"],["C5","523.25"],["E5","659.25"],["F5","698.46"],["F#4","369.99"],["D4","293.66"],["G4","392"],["B4","493.88"],["D#5","622.25"],["F#5","739.99"],["A5","880"]];

shuffle(notes);

let audioSouns=["00","01","02","03","10","11","12","13","20","21","22","23"];
for(i=0;i<audioSouns.length;i++){
  notes[i][2]=audioSouns[i];
}
let freq=440;

function play(val){
  console.log(val[5]+val[6]);
  for(i in notes){
      //console.log(i);
    if (notes[i][2]==val[5]+val[6]){
      console.log(notes[i][1]);
      freq=notes[i][1];
    }
  };
  //Web Audio API

    let audioContext;
    
    try {
      audioContext =
        new (window.AudioContext || window.webkitAudioContext)();
    } catch (error) {
      window.alert(
        `Sorry, but your browser doesn't support the Web Audio API!`
      );
    }
    
    if (audioContext !== undefined) {
        const oscillator = audioContext.createOscillator();
        oscillator.connect(audioContext.destination);
        oscillator.frequency.setValueAtTime(freq, audioContext.currentTime);
        oscillator.start(audioContext.currentTime );
        oscillator.stop(audioContext.currentTime + .5);
    }
}


//button container

let viewport=document.createElement('div');
viewport.className="viewport";
document.body.appendChild(viewport);

//buttons
for(i=0;i<3;i++){
    let row=document.createElement('div');
    row.className="row";
    viewport.appendChild(row);
    for(j=0;j<4;j++){
        let button=document.createElement('button');
        button.className="buttons";
        button.id="audio"+String(i)+j;
        button.onclick=function play1(){
            play(button.id);
            playSound(button.id);
        };
        row.appendChild(button);
    }
}

let nextNoteIndex=0;
let seq=["A4", "C5", "G4","B4", "A4","A4","A4", "F4", "C5", "A4", "E5", "E5", "E5", "F5","C5", "A4", "F#4", "C5", "A4", "F#4", "C5", "A4"];

function changeBckgr(){
  for (i in notes){
    if (notes[i][0]===seq[nextNoteIndex]){
      let x=document.getElementById("audio"+notes[i][2]);
      x.style.backgroundColor="lightyellow";
      x.classList.add("active");
      x.classList.remove("idle");
    }
  }
}

changeBckgr();

  function playSound(val){
  console.log(document.getElementById(val).id);
  document.getElementById(val).classList.add("idle");
  document.getElementById(val).classList.remove("active");
  document.getElementById(val).style.backgroundColor="lightgrey";
  nextNoteIndex++;
  changeBckgr();

}